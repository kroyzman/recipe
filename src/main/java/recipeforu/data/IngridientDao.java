package recipeforu.data;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import recipeforu.logic.Ingridient;
import recipeforu.logic.Recipe;

public interface IngridientDao extends CrudRepository<Ingridient, Long> {
	
	@Query("SELECT i FROM Ingridient i WHERE i.name LIKE %:name%")
	public Ingridient[] getIngridientsByName(@Param("name") String name);
	
	@Query("SELECT i FROM Ingridient i WHERE i.name = :name")
	public Ingridient getIngridientByName(@Param("name") String name);
	
	@Query("SELECT i FROM Ingridient i WHERE i.name LIKE :name")
	public Ingridient getIngridientsByExactName(@Param("name") String name);
	
	@Override
    List<Ingridient> findAll();
}
