package recipeforu.data;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import recipeforu.logic.Instruction;
import recipeforu.logic.Instruction.RecipeCourse;

public interface InstructionDao extends CrudRepository<Instruction, Long>{
		
	@Query("SELECT i FROM Instruction i WHERE i.course IN :myParams")
	public List<Instruction> getInstructionByCatagory(@Param("myParams") List<RecipeCourse> recipeCourse);
}
