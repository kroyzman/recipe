package recipeforu.data;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import recipeforu.logic.Ingridient;
import recipeforu.logic.Instruction;
import recipeforu.logic.Recipe;

public interface RecipeDao extends PagingAndSortingRepository<Recipe, Long>
//extends CrudRepository<Recipe, Long> {
{
	@Query("SELECT r FROM Recipe r WHERE r.title LIKE %:title%")
	public List<Recipe> getRecipesByTitle(@Param("title") String title,Pageable first);
	
//	@Query("SELECT r FROM Recipe r WHERE")
//	public List<Recipe> getRecipesByCatagory(@Param("myParams") String[] params);
	
	@Override
    List<Recipe> findAll();

	@Query("SELECT r FROM Recipe r WHERE r.instruction IN :instructionsList")
	public List<Recipe> getRecipesByInstructions(@Param("instructionsList") List<Instruction> instructionsList, Pageable first);

	@Query("SELECT r FROM Recipe r WHERE r.instruction IN :instructionsList AND r.title LIKE %:title%")
	public List<Recipe> getRecipesByInstructionsAndTitle(@Param("instructionsList") List<Instruction> instructionsList, @Param("title") String title,Pageable first);
	
	// Recipe[] getRecipesByTitle(PageRequest of, String title);

	//public Recipe[] getRecipesByTitle(PageRequest of, String title);
	
	 //@Query(value = "SELECT a.* FROM numberentity n INNER JOIN articleentity a on n.id = (select n.id from numberentity n where n.volume = ?1 AND n.numero = ?2)AND n.id = a.numbers_id order by a.article asc", nativeQuery=true)
	
	//@Query("SELECT r from Recipe r inner join r.recipes_ingridients i on r.id = i.recipe_id")
	@Query("SELECT r from Recipe r")
	public List<Recipe> getRecipesByIngridients(List<Ingridient> ingridients);

	@Query("SELECT COUNT(r) FROM Recipe r WHERE r.title LIKE %:title%")
	public long getRecipesCountByTitle(String title);
	

	//get recipes by ingridiens- sql special query 
}
