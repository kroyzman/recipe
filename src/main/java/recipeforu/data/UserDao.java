package recipeforu.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import recipeforu.logic.Ingridient;
import recipeforu.logic.UserDetails;

public interface UserDao extends CrudRepository<UserDetails, String> {
	
	@Query("SELECT count(r) from UserRatesRecipe r where r.ratedRecipeId = :recipeId")
	Long getRatedRecipeCount(@Param("recipeId") long recipeId);
	
}
