package recipeforu.layout;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

//import py4j.GatewayServer;
import recipeforu.logic.Ingridient;

public class IngridientEntryPoint {

    private List<Ingridient> ingridients;
    
    
    public IngridientEntryPoint(List<Ingridient> ingridients) {
		super();
		this.ingridients = ingridients;
	}

	public IngridientEntryPoint() {
      ingridients = new ArrayList<>();
    }

    public List getList() {
        return ingridients;
    }

//    public static void main(String[] args) {
//        //Here, the 'entry point' object is StackEntryPoint, 
//        // which creates a new Stack object. This wraps around a 
//        // List and has pop() and push(String element) 
//        // methods to manipulate the List
//        GatewayServer gatewayServer = new GatewayServer();
//        gatewayServer.start();
//    }

}