package recipeforu.layout;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.jni.OS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import py4j.GatewayServer;
import recipeforu.data.IngridientDao;
import recipeforu.data.InstructionDao;
import recipeforu.data.RecipeDao;
import recipeforu.logic.Ingridient;
import recipeforu.logic.IngridientService;
import recipeforu.logic.Instruction;
import recipeforu.logic.Recipe;
import recipeforu.logic.RecipeIngridient;
import recipeforu.logic.UserDetails;
import recipeforu.logic.jpa.UserService;

@RestController
public class IngridientWebController {

	private IngridientDao ingridientRepository;
	private RecipeDao recipeRepository;
	private InstructionDao instructionRepository;
	private IngridientService ingridientService;
	private UserService userSerivce;
	
	
	@Autowired
	public void setUserSerivce(UserService userSerivce) {
		this.userSerivce = userSerivce;
	}

	public IngridientService getIngridientService() {
		return ingridientService;
	}

	@Autowired
	public void setIngridientService(IngridientService ingridientService) {
		this.ingridientService = ingridientService;
	}

	@Autowired
	public void setInstructionRepository(InstructionDao instructionRepository) {
		this.instructionRepository = instructionRepository;
	}

	@Autowired
	public void setRecipeRepository(RecipeDao recipeRepository) {
		this.recipeRepository = recipeRepository;
	}

	@Autowired
	public void setIngridientRepository(IngridientDao ingridientRepository) {
		this.ingridientRepository = ingridientRepository;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/parse_yummuly", produces = MediaType.APPLICATION_JSON_VALUE)
	public String FetchApiToDatabase() {

		JsonParser jsonmapper = new JsonParser();
		jsonmapper.mapper(ingridientRepository, recipeRepository, instructionRepository);

		return "Saved";
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.POST, path = "/recipeforu/reciept_upload")
	public UserDetails scanReciept(@RequestParam("file") MultipartFile file, @RequestParam("user") String userEmail) throws Exception {
		System.out.println(userEmail);
		
		OCRservice oService = new OCRservice();
		List<String> ingridientsListFromOCR = oService.parseIngridientsFromReciept(file);
		ArrayList<Ingridient> ingridientsToReturn = new ArrayList<>();
		
		System.out.println(ingridientsListFromOCR);
		
		for(int i = 0 ; i < ingridientsListFromOCR.size() ; i++) {
			Ingridient currentIngridient = ingridientService.getIngridientsByExactName(ingridientsListFromOCR.get(i).toString());
			if(currentIngridient != null) {
				System.out.println(ingridientsListFromOCR.get(i).toString() + " Not Null");
				ingridientsToReturn.add(currentIngridient);
				UserDetails currentUser = userSerivce.getUserByEmail(userEmail);
				if(!currentUser.getIngridients().contains(currentIngridient)) {
					currentUser.getIngridients().add(currentIngridient);
				}	
			}
		}
		
		userSerivce.updateUser(userEmail, userSerivce.getUserByEmail(userEmail));
		System.out.println(userSerivce.getUserByEmail(userEmail).getIngridients());	
		return userSerivce.getUserByEmail(userEmail);

	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/ingridients/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Ingridient[] getIngridientByName(@PathVariable("name") String name) {

		Ingridient[] ingridients;
		ingridients = ingridientService.getIngridientsByName(name);

		return ingridients;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/ingridients", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Ingridient> getAllIngridients() throws Exception {

		List<Ingridient> ingridients;
		ingridients = ingridientService.getAllIngridients();
		System.out.println(ingridients);
		return ingridients;
	}

}
