package recipeforu.layout;

import java.util.ArrayList;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import py4j.GatewayServer;
import recipeforu.data.IngridientDao;
import recipeforu.data.InstructionDao;
import recipeforu.data.RecipeDao;

import recipeforu.logic.Ingridient;
import recipeforu.logic.IngridientService;
import recipeforu.logic.Instruction;
import recipeforu.logic.Instruction.RecipeCourse;
import recipeforu.logic.Instruction.RecipeCuisine;
import recipeforu.logic.InstructionService;
import recipeforu.logic.Recipe;
import recipeforu.logic.RecipeIngridient;
import recipeforu.logic.RecipeService;

@Component
public class JsonParser {
	
	public void mapper(IngridientDao ingridientRepository, RecipeDao recipeRepository, InstructionDao instructionRepository) {
//		String[] food = { "onion+soup", "pizza", "salad", "hamburger" };
		String[] food = { "kid", "cocktail", "pasta", "shakshuka" };
		//String[] food = { "onion+soup"};

		try {
			RestTemplate restTemplate = new RestTemplate();
			for (int i = 0; i < food.length; i++) {

				ResponseEntity<String> responseMatchesJson = restTemplate.getForEntity(
						"http://api.yummly.com/v1/api/recipes?_app_id=a1f532d1&_app_key=23e064a7f624a0e3ff25dc27b47575a5&q=" + food[i],
						String.class);

				ObjectMapper mapper = new ObjectMapper();
				JsonNode root = mapper.readTree(responseMatchesJson.getBody());
				System.out.println(responseMatchesJson.getBody());

				// get the list of the matching recipes
				JsonNode recipesArr = root.path("matches");
				for (final JsonNode objNode : recipesArr) {
					
					Recipe currentRecipe = new Recipe();
					Instruction recipeInstruction = new Instruction();

					// parse cuisine and course
					parseCuisineCourse(objNode, recipeInstruction);
					
					// parse prep time
					JsonNode recipePrepTime = objNode.path("totalTimeInSeconds");
					recipeInstruction.setPrepTimeInSeconds(recipePrepTime.asInt());
					
					// parse recipe img
					JsonNode recipeImg = objNode.path("smallImageUrls");
					parseRecipeImgs(recipeImg, currentRecipe);
					
					// parse recipe name
					JsonNode recipeTitle = objNode.path("recipeName");
					currentRecipe.setTitle(recipeTitle.asText());

					// get recipe by id from api
					JsonNode recipeId = objNode.path("id");
					ResponseEntity<String> responseId = restTemplate
							.getForEntity(
									"http://api.yummly.com/v1/api/recipe/" + recipeId.asText()
											+ "?_app_id=a1f532d1&_app_key=de4976502ce60e437bdd48db69b659f7",
									String.class);

					mapper = new ObjectMapper();
					JsonNode recipe = mapper.readTree(responseId.getBody());

					// parse numberOfServings
					recipeInstruction.setNumberOfServings(recipe.path("numberOfServings").asInt());
					
					// parse steps
					ArrayList<String> steps = parseRecipeSteps(recipe);
					recipeInstruction.setSteps(steps);
					
					// parse rating
					currentRecipe.setAvgRating(recipe.path("rating").asInt());
					
					System.out.println("before saving");
					instructionRepository.save(recipeInstruction);
					System.out.println("after saving");
					currentRecipe.setInstruction(recipeInstruction);
					recipeRepository.save(currentRecipe);
					
					
					// parse ingridients for each recipe
					JsonNode recipeIngridients = objNode.path("ingredients");
					parseIngridients(recipeIngridients, currentRecipe, ingridientRepository);
					

				}
			}

		} catch (Exception e) {

		}
		
	}

	private ArrayList<String> parseRecipeSteps(JsonNode recipe) {
		ArrayList<String> steps = new ArrayList<>();
		JsonNode lines = recipe.path("ingredientLines");

		for (final JsonNode step : lines) {
			steps.add(step.asText());
		}
		return steps;
	}

	private void parseRecipeImgs(JsonNode objNode, Recipe currentRecipe) {
		for (final JsonNode img : objNode) {
			currentRecipe.getImg().add(img.asText());
		}

	}

	private void parseIngridients(JsonNode objNode, Recipe currentRecipe, IngridientDao ingridientRepository) throws Exception {

		for (final JsonNode ingridient : objNode) {
			boolean isContained = false;
			String lemmitaizerIngridientName = ingridientLemmitaizer(ingridient.asText());
			Ingridient currentIngridient = checkIngridient(lemmitaizerIngridientName, ingridientRepository);

			if (currentIngridient == null) {
				// create new ingridient
				currentIngridient = new Ingridient(lemmitaizerIngridientName);
				
			}
			
			
			Iterator it = currentRecipe.getRecipeIngridients().iterator();
			while(it.hasNext()){
				if(((RecipeIngridient)it.next()).getIngridient().getName().equalsIgnoreCase(currentIngridient.getName())) {
					System.out.println("Contains --> " + currentIngridient.getName());
					isContained = true;
					break;
				}
		     }
				
			if(!isContained) {
				// add ingridient to the recipe
				RecipeIngridient recipeIngridient = new RecipeIngridient();
				recipeIngridient.setIngridient(currentIngridient);
				recipeIngridient.setRecipe(currentRecipe);
				currentRecipe.getRecipeIngridients().add(recipeIngridient);
				
				
				// currentIngridient.getRecipeIngridients().add(recipeIngridient);
				
				ingridientRepository.save(currentIngridient);
			}
				
		
			
		}

	}
	
	private String ingridientLemmitaizer(String ingridient) {
		GatewayServer.turnLoggingOff();
		GatewayServer server = new GatewayServer();
		server.start();
		PythonService pythonSide = (PythonService) server
				.getPythonServerEntryPoint(new Class[] { PythonService.class });
		
		String[] returnedResult = ingridient.split(" ");
		String lemmitaizerIngridientResult = pythonSide.lemmatizer(returnedResult[returnedResult.length - 1]);
		System.out.println("Result: " + lemmitaizerIngridientResult);
		
		
	
		server.shutdown();
		

		return lemmitaizerIngridientResult;
	
	}

	private Ingridient checkIngridient(String ingridient, IngridientDao ingridientRepository) {
		Ingridient currentIngridient = ingridientRepository.getIngridientByName(ingridient);
		return currentIngridient;
	}

	public void parseCuisineCourse(JsonNode objNode, Instruction recipeInstruction) {
		JsonNode recipeCuisine = objNode.path("attributes").path("cuisine");
		for (final JsonNode cuisineNode : recipeCuisine) {
			String cuisine = checkCuisine(cuisineNode.asText());
			if (cuisine != null) {
				recipeInstruction.setCuisine(RecipeCuisine.valueOf(cuisine));
			}
		}

		JsonNode recipeCourse = objNode.path("attributes").path("course");
		for (final JsonNode courseNode : recipeCourse) {
			String course = checkCourse(courseNode.asText());
			if (course != null) {
				recipeInstruction.setCourse(RecipeCourse.valueOf(course));
			}
		}
	}

	public String checkCourse(String course) {
		for (int i = 0; i < RecipeCourse.values().length; i++) {
			if (course.equalsIgnoreCase(RecipeCourse.values()[i].name().replace("_", " "))) {
				return RecipeCourse.values()[i].name();
			}
		}
		return null;
	}

	public String checkCuisine(String cuisine) {
		for (int i = 0; i < RecipeCuisine.values().length; i++) {
			if (cuisine.equalsIgnoreCase(RecipeCuisine.values()[i].name().replace("_", " "))) {
				return RecipeCuisine.values()[i].name();
			}
		}
		return null;
	}
}
