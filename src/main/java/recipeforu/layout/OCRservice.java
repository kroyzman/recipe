package recipeforu.layout;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.web.multipart.MultipartFile;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import py4j.GatewayServer;
import recipeforu.logic.Ingridient;

public class OCRservice {
	GatewayServer server = new GatewayServer();
	
	public List<String> parseIngridientsFromReciept(MultipartFile file) throws IllegalStateException, IOException {
		Tesseract tesseract = new Tesseract();
		try {
			File image = new File(file.getOriginalFilename());
			file.transferTo(image);
			tesseract.setDatapath("C:\\Users\\kroyzman\\Desktop");
			String text = tesseract.doOCR(image);
			
			List<String> ingridientsListFromReciept = getIngridientsFromText(text);
			
			ingridientsListFromReciept = lemmitaizeOcrIngridients(ingridientsListFromReciept);
			
			return ingridientsListFromReciept;

		} catch (TesseractException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	private List<String> lemmitaizeOcrIngridients(List<String> ingridientsListFromReciept) {
		ArrayList<String> finalLemmitaizeIngridientsList = new ArrayList<>();
		GatewayServer.turnLoggingOff();
//		server.start();
		PythonService pythonSide = (PythonService) server
				.getPythonServerEntryPoint(new Class[] { PythonService.class });
		
		for(int i = 0 ; i < ingridientsListFromReciept.size() ; i++) {
			finalLemmitaizeIngridientsList.add(pythonSide.lemmatizer(ingridientsListFromReciept.get(i).toString().toLowerCase()));
		}
		
//		server.shutdown();
		
		return finalLemmitaizeIngridientsList;
	}

	private List<String> getIngridientsFromText(String text) {
		ArrayList<String> tempList = new ArrayList<>();
		String[] lines = text.split("\n");
		
		
		for(int i = 1 ; i < lines.length ; i++) {
			String[] wordsInLine = lines[i].split(",");
			wordsInLine = wordsInLine[0].split(" ");
			if(!isNumeric(wordsInLine[0])) {
				tempList.add(wordsInLine[0]);
			}
			
		}
		return tempList;
	}
	
	private static boolean isNumeric(String str) {
		try {
	        int d = Integer.parseInt(str);
	    } catch (NumberFormatException | NullPointerException nfe) {
	        return false;
	    }
	    return true;
		}
}
