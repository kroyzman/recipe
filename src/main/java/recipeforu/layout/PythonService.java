package recipeforu.layout;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import recipeforu.logic.UserDetails;

public interface PythonService {
    public List recommendEvent(List<UserDetails> users, long userRecommendId);
    public String lemmatizer(String ingridient);
    public void openGateway();
}
