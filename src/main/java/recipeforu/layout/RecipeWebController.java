package recipeforu.layout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.dialect.identity.SybaseAnywhereIdentityColumnSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import py4j.GatewayServer;
import recipeforu.logic.CountUserWatchRecipe;
import recipeforu.logic.Ingridient;
import recipeforu.logic.IngridientService;
import recipeforu.logic.Instruction;
import recipeforu.logic.Instruction.RecipeCourse;
import recipeforu.logic.InstructionService;
import recipeforu.logic.Recipe;
import recipeforu.logic.RecipeIngridient;
import recipeforu.logic.RecipeService;
import recipeforu.logic.UserDetails;
import recipeforu.logic.UserRatesRecipe;
import recipeforu.logic.jpa.UserService;

@RestController
public class RecipeWebController {

	private RecipeService recipeService;
	private IngridientService ingridientService;
	private InstructionService instructionService;
	private UserService userService;
	GatewayServer server = new GatewayServer();
	//private ListenerPython ipython = new ListenerPython();

//	@Autowired
//	public void setRecipeIngridientsService(RecipeIngridientsService recipeIngridientsService) {
//		this.recipeIngridientsService = recipeIngridientsService;
//	}
	
	

	public IngridientService getIngridientService() {
		return ingridientService;
	}

	public UserService getUserService() {
		return userService;
	}
	
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	public void setInstructionService(InstructionService instructionService) {
		this.instructionService = instructionService;
	}

	@Autowired
	public void setIngridientService(IngridientService ingridientService) {
		this.ingridientService = ingridientService;
	}

	@Autowired
	public void setRecipeService(RecipeService recipeService) {
		this.recipeService = recipeService;
	}

	public RecipeService getRecipeService() {
		return recipeService;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/recipeforu/recipe/add", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Recipe addNewRecipe(@RequestBody Recipe recipe) throws Exception {

		return recipeService.addNewRecipe(recipe);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/recieps/count/{title}", produces = MediaType.APPLICATION_JSON_VALUE)
	public long getTotalCountOfRecipesByTitle(@PathVariable("title") String title)	{
		long countRecipes;
		
		countRecipes = recipeService.getRecipesCountByTitle(title);
		System.out.println("count recipe by title -> " + countRecipes);
		return countRecipes;
		
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/recieps/count", produces = MediaType.APPLICATION_JSON_VALUE)
	public long getTotalCountOfRecipes(@RequestParam(value = "myParam", required = false) String[] myParams)
			throws Exception {
		long countRecipes;

		if (myParams == null) {

			countRecipes = recipeService.getRecipesCount();
		} else {
			List<Instruction> instructionsList = new ArrayList<>();
			List<RecipeCourse> courses = new ArrayList<>();

			for (int i = 0; i < myParams.length; i++) {

				courses.add(Instruction.RecipeCourse.valueOf(myParams[i]));
			}

			instructionsList = instructionService.getInstructionByCatagory(courses);
			countRecipes = instructionsList.size();
		}

		System.out.println(countRecipes);
		return countRecipes;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/recieps", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Recipe> viewAllRecipes(@RequestParam(name = "page", required = false, defaultValue = "0") int page,
			@RequestParam(name = "size", required = false, defaultValue = "9") int size,
			@RequestParam(value = "myParam", required = false) String[] myParams) throws Exception {

		List<Recipe> reciepList = new ArrayList<>();
		List<Instruction> instructionsList = new ArrayList<>();
		List<RecipeCourse> courses = new ArrayList<>();

		if (myParams == null) {
			reciepList = recipeService.getAllRecipes(page, size);
		} else {
			for (int i = 0; i < myParams.length; i++) {
				courses.add(Instruction.RecipeCourse.valueOf(myParams[i]));
			}

			instructionsList = instructionService.getInstructionByCatagory(courses);
			for (int i = 0; i < instructionsList.size(); i++) {
				System.out.println(instructionsList.get(i));

			}
			
			try {
				reciepList = recipeService.getRecipesByInstructions(page, size, instructionsList);
			} catch (Exception e) {
				e.printStackTrace();
				return null;		}
		}

		System.out.println(reciepList);
		return reciepList;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/recieps/{title}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Recipe> viewReciepsByTitle(@PathVariable("title") String title,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page,
			@RequestParam(name = "size", required = false, defaultValue = "9") int size,
			@RequestParam(value = "myParam", required = false) String[] myParams) throws Exception {

		List<Recipe> reciepList = new ArrayList<>();
		System.err.println(title);

		List<Instruction> instructionsList = new ArrayList<>();
		List<RecipeCourse> courses = new ArrayList<>();

		if (myParams == null) {
			reciepList = recipeService.getRecipesByTitle(title, page, size);
		} else {
			for (int i = 0; i < myParams.length; i++) {

				courses.add(Instruction.RecipeCourse.valueOf(myParams[i]));
			}

			instructionsList = instructionService.getInstructionByCatagory(courses);
			for (int i = 0; i < instructionsList.size(); i++) {
				System.out.println(instructionsList.get(i));

			}

			reciepList = recipeService.getRecipesByInstructionsAndTitle(page, size, instructionsList, title);
		}

		System.out.println(reciepList);
		return reciepList;

	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/reciep/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Recipe getReciepById(@PathVariable("id") int id) throws Exception {

		Recipe reciep = recipeService.getReciepById(id);
		return reciep;
	}

	
	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/recipe_all", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Recipe> getAllRecipes() throws Exception {

		List recipeList = this.recipeService.getAllRecipesWithoutPagination();
		return recipeList;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/count_views/{email}/{recipeId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void countViews(@PathVariable("email") String email, @PathVariable("recipeId") int recipeId) throws Exception {
    	boolean isRatedBefore = false;
		UserDetails currentUser = this.userService.getUserByEmail(email);
    	Recipe currentRecipe = this.recipeService.getReciepById(recipeId);
    	CountUserWatchRecipe views = null;
    	
    	System.out.println("current User in count views -> " + currentUser.getEmail());
		this.recipeService.increaseCountViews(currentUser.getRecommendUserId(), recipeId);
		
//		for(int i = 0 ; i < currentRecipe.getViews().size() ; i++) {
//			isRatedBefore = false;
//			if(currentRecipe.getViews().get(i).getUserId() == currentUser.getRecommendUserId()) {
//				views = currentRecipe.getViews().get(i);
//				if(currentRecipe.getViews().get(i).getCountViews() == 2) {
//					for(int j = 0 ; j < currentUser.getUserRatesRecipes().size() ; j++) {
//						if(currentUser.getUserRatesRecipes().get(j).getRatedRecipeId() == recipeId) {
//							isRatedBefore = true;
//							if (currentUser.getUserRatesRecipes().get(j).getUserRate() < 5) {
//								currentUser.getUserRatesRecipes().get(j)
//										.setRateId(currentUser.getUserRatesRecipes().get(j).getRatedRecipeId() + 1);
//								this.userService.updateUser(currentUser.getEmail(), currentUser);
//							}
//						}
//					}
//					
//				} else if(currentRecipe.getViews().get(i).getCountViews() == 3) {
//					for(int j = 0 ; j < currentUser.getUserRatesRecipes().size() ; j++) {
//						if(currentUser.getUserRatesRecipes().get(j).getRatedRecipeId() == recipeId) {
//							isRatedBefore = true;
//							if(currentUser.getUserRatesRecipes().get(j).getUserRate() < 5) {
//								currentUser.getUserRatesRecipes().get(j).setRateId(currentUser.getUserRatesRecipes().get(j).getRatedRecipeId() + 1);
//								this.userService.updateUser(currentUser.getEmail(),currentUser);
//							}
//						}
//					}
//				} else if(currentRecipe.getViews().get(i).getCountViews() == 4) {
//					for(int j = 0 ; j < currentUser.getUserRatesRecipes().size() ; j++) {
//						if(currentUser.getUserRatesRecipes().get(j).getRatedRecipeId() == recipeId) {
//							isRatedBefore = true;
//							if (currentUser.getUserRatesRecipes().get(j).getUserRate() < 5) {
//								currentUser.getUserRatesRecipes().get(j)
//										.setRateId(currentUser.getUserRatesRecipes().get(j).getRatedRecipeId() + 1);
//								this.userService.updateUser(currentUser.getEmail(), currentUser);
//							}
//						}
//					}
//				}
//			}
//		}
		
		if(!isRatedBefore) {
			boolean isContained = false;
			System.out.println("IN HERHERHEHRHERHER " + currentUser.getRecommendUserId());
			views = new CountUserWatchRecipe();
			views.setCountViews(1);
			views.setUserId(currentUser.getRecommendUserId());
			
			currentRecipe.getViews().add(views);
				for(int i = 0 ; i < currentUser.getUserRatesRecipes().size() ; i++)
				{
					if(currentUser.getUserRatesRecipes().get(i).getRatedRecipeId() == currentRecipe.getId()) {
						if(currentUser.getUserRatesRecipes().get(i).getUserRate() < 5) {
							currentUser.getUserRatesRecipes().get(i).setUserRate(currentUser.getUserRatesRecipes().get(i).getUserRate() + 1);
							this.userService.updateUser(currentUser.getEmail(), currentUser);
						}
						
						isContained = true;
					}
				}
				
				if(!isContained) {
					currentUser.getUserRatesRecipes().add(new UserRatesRecipe(2, currentRecipe.getId()));
					this.userService.updateUser(currentUser.getEmail(), currentUser);
				}
				
				//			this.recipeService.save(currentRecipe);
			}
			
		}
		
		
		
		
		
		
	

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.POST, path = "/recipeforu/recipe_ingridients", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, List<Recipe>> getRecipesByIngridients(@RequestBody Map<String, List<Ingridient>> ingridients)
			throws Exception {

		Map<String, List<Recipe>> returnedRecipeLists = new HashMap<>();
		List<Recipe> reciepList = new ArrayList<>();
		List<Recipe> returnedRecipesByIngridients = new ArrayList<>();
		List<Recipe> returnedRecipesByIngridientsMissingOneIngridient = new ArrayList<>();
		List<Recipe> returnedRecipesByIngridientsMissingTwoIngridients = new ArrayList<>();
		List<Recipe> returnedRecipesByIngridientsMissingThreeIngridients = new ArrayList<>();
		List<Ingridient> userIngridients = ingridients.get("ingridients");
		boolean isContained = false;
		int countContained = 0;
		int ingridientsByRecipeSize;

		reciepList = recipeService.getRecipesByIngridients(ingridients.get("ingridients"));

		for (int i = 0; i < reciepList.size(); i++) {
			countContained = 0;
			ingridientsByRecipeSize = reciepList.get(i).getRecipeIngridients().size();
			for (RecipeIngridient ri : reciepList.get(i).getRecipeIngridients()) {
				isContained = false;
				for (int j = 0; j < userIngridients.size(); j++) {
					if (userIngridients.get(j).getId() == ri.getIngridient().getId()) {
						isContained = true;
						countContained++;
						break;
					}
				}
				if (!isContained) {
					System.err.println("Error not contain - " + ri.getIngridient().getName());
				}

			}

			if (countContained == ingridientsByRecipeSize) {
				returnedRecipesByIngridients.add(reciepList.get(i));
			}
			if (countContained == ingridientsByRecipeSize - 1) {
				returnedRecipesByIngridientsMissingOneIngridient.add(reciepList.get(i));
			}
			if (countContained == ingridientsByRecipeSize - 2) {
				returnedRecipesByIngridientsMissingTwoIngridients.add(reciepList.get(i));
			}
			if (countContained == ingridientsByRecipeSize - 3) {
				returnedRecipesByIngridientsMissingThreeIngridients.add(reciepList.get(i));
			}
		}

		returnedRecipeLists.put("Matched", returnedRecipesByIngridients);
		returnedRecipeLists.put("MissingOne", returnedRecipesByIngridientsMissingOneIngridient);
		returnedRecipeLists.put("MissingTwo", returnedRecipesByIngridientsMissingTwoIngridients);
		returnedRecipeLists.put("MissingThree", returnedRecipesByIngridientsMissingThreeIngridients);

		return returnedRecipeLists;
	}
	
	

	// ipython.py(new IngridientEntryPoint(ingridients));
}
