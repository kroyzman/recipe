package recipeforu.layout;
import java.util.ArrayList;
import java.util.List;
import py4j.GatewayServer;
import recipeforu.logic.UserDetails;
import recipeforu.logic.jpa.UserService;


public class RecommendEvent {
	private boolean isUp;
	
	
	public RecommendEvent(boolean isUp) {
		this.isUp = isUp;
	}


	public List recommendEvent(GatewayServer server, UserService userService, long userRecommendId) throws Exception{
        
        List<UserDetails> users = userService.getAllUsers();
       
 
        
		PythonService pythonSide = (PythonService) server
				.getPythonServerEntryPoint(new Class[] { PythonService.class });
		
		List list = pythonSide.recommendEvent(users, userRecommendId);

		return list;
	}   

	
	
}
