package recipeforu.layout;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py4j.GatewayServer;
import recipeforu.logic.NewUserForm;
import recipeforu.logic.Recipe;
import recipeforu.logic.RecipeService;
import recipeforu.logic.UserDetails;
import recipeforu.logic.UserRatesRecipe;
import recipeforu.logic.jpa.UserService;

@RestController
public class UserWebController {

	/** Class Attributes */
	private UserService userService;
	private RecipeService recipeService;
	private boolean isUp = false;
	GatewayServer server = new GatewayServer();
	
	/** Class Getters & Setters */
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	public void setRecipeService(RecipeService recipeService) {
		this.recipeService = recipeService;
	}

	/** Class Methods */
	// POST /recipeforu/users
	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.POST, path = "/recipeforu/users/add", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserDetails register(@RequestBody NewUserForm newUserForm) throws Exception {

		// UserDetails newUser= new UserDetails(newUserForm.getUserName(),
		// newUserForm.getPassword(), newUserForm.getEmail());
		System.out.println(newUserForm.getUserName());
		UserDetails rv = this.userService.addNewUser(newUserForm.getEmail(), newUserForm.getUserName(),
				newUserForm.getPassword());
		System.out.println(rv.getEmail() + ", " + rv.getPassword() + ", " + rv.getUserName());
		return rv;
	}

	// GET /recipeforu/users/login/{email}/{password}

	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/users/login/{email}/{password}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "http://localhost:3000")
	public UserDetails login(@PathVariable("email") String email, @PathVariable("password") String password)
			throws Exception {
		UserDetails user = userService.login(email, password);
		return user;

	}

	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/users/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "http://localhost:3000")
	public UserDetails getUser(@PathVariable("email") String email) throws Exception {

		UserDetails user = userService.getUserByEmail(email);
		return user;

	}

	// PUT /recipeforu/users/{email}
	@RequestMapping(method = RequestMethod.PUT, path = "/recipeforu/users/update/{email}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "http://localhost:3000")
	public void updateUser(@PathVariable("email") String email, @RequestBody UserDetails userUpdated) throws Exception {
		userService.updateUser(email, userUpdated);
	}

	// POST /recipeforu/users
	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.POST, path = "/recipeforu/users/rating/{email}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateUserRating(@PathVariable("email") String email, @RequestBody UserRatesRecipe userRatesRecipe)
			throws Exception {

		boolean isContained = false;
		UserDetails rv = this.userService.getUserByEmail(email);
		List<UserRatesRecipe> list = rv.getUserRatesRecipes();

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getRatedRecipeId() == userRatesRecipe.getRatedRecipeId()) {
				isContained = true;
				list.get(i).setUserRate(userRatesRecipe.getUserRate());
				break;
			}
		}

		if (!isContained) {
			list.add(userRatesRecipe);
		}

		rv.setUserRatesRecipes(list);
		this.userService.updateUser(email, rv);

	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.POST, path = "/recipeforu/recipe/{recipe_id}/{rating}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Recipe updateRecipeRatings(@PathVariable("recipe_id") Long recipeId, @PathVariable("rating") int rating,
			@RequestBody UserDetails user) throws Exception {

		System.out.println("recipe id - " + recipeId);
		Long returnedCount = this.userService.getRatedRecipeCount(recipeId);

		Recipe recipe = this.recipeService.getReciepById(recipeId.intValue());

		return this.recipeService.updateRating(recipe, rating, user, returnedCount);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/recipeforu/users/recommend_event/{user_id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "http://localhost:3000")
	public List recommendEvent(@PathVariable("user_id") Long userRecommendId) throws Exception {

		GatewayServer.turnLoggingOff();
		if(!this.isUp) {
			server.start();
			System.out.println("IS UPPPP");
			this.isUp = !this.isUp;
		}

		
		

		ArrayList<Recipe> returnedRecipes = new ArrayList<>();
		RecommendEvent event = new RecommendEvent(isUp);
		
		List itemsIdsList = event.recommendEvent(server, userService, userRecommendId);
		System.out.println(itemsIdsList);

		for (int i = 0; i < itemsIdsList.size(); i++) {
			returnedRecipes.add(recipeService.getReciepById(Integer.parseInt((String) itemsIdsList.get(i))));
		}

		System.out.println(returnedRecipes);
		return returnedRecipes;
	}

}
