package recipeforu.logic;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CountUserWatchRecipe {

	private Long id;
	private long userId;
	private int countViews;
	
	
	
	public CountUserWatchRecipe() {
		super();
	}
	public CountUserWatchRecipe(Long id, long userId, int countViews) {
		super();
		this.id = id;
		this.userId = userId;
		this.countViews = countViews;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getCountViews() {
		return countViews;
	}
	public void setCountViews(int countViews) {
		this.countViews = countViews;
	}
	
	
}
