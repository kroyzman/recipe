package recipeforu.logic;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnore;

import recipeforu.logic.jpa.IdGenerator;

@Entity
public class Ingridient {
	
	enum IngridientType {
		DAIRY, VEGETABLE, FRUITS, SPICE, MEAT, 
		SAUCE;
	}
	
	
	private Long id;
	private String name;
	private Set<RecipeIngridient> recipeIngridients = new HashSet<>();
	private IngridientType type;
	
	public Ingridient() {
	}
	
	
	public Ingridient(Long id, String name, Set<RecipeIngridient> recipeIngridients, IngridientType type) {
		super();
		this.id = id;
		this.name = name;
		this.recipeIngridients = recipeIngridients;
		this.type = type;
	}


	public Ingridient(String name, Set<RecipeIngridient> recipeIngridients, IngridientType type) {
		this.name = name;
		this.recipeIngridients = recipeIngridients;
		this.type = type;
	}
	
	
	
	public Ingridient(String name) {
		super();
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	@OneToMany(mappedBy = "ingridient")
	@JsonIgnore
	public Set<RecipeIngridient> getRecipeIngridients() {
		return recipeIngridients;
	}

	public void setRecipeIngridients(Set<RecipeIngridient> recipeIngridients) {
		this.recipeIngridients = recipeIngridients;
	}

	public IngridientType getType() {
		return type;
	}

	public void setType(IngridientType type) {
		this.type = type;
	}

}
