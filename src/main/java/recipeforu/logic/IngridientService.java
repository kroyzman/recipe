package recipeforu.logic;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public interface IngridientService {

	public Ingridient addNewIngridient(Ingridient ingridient) throws Exception;
	public Ingridient[] getIngridientsByName(String name);
	public Ingridient getIngridientById(long id) throws Exception;
	public List<Ingridient> getAllIngridients() throws Exception;
	public Ingridient getIngridientsByExactName(String name);

}
