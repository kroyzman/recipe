package recipeforu.logic;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Instruction {

	public enum RecipeCuisine {
		American, Italian, Asian, Mexican, Southern_Soul_Food, French, Southwestern, Barbecue, Indian, Chinese, Cajun_Creole, English, Mediterranean, Greek, Spanish, German, Thai, Moroccan, Irish, Japanese, Cuban, Hawaiin, Swedish, Hungarian, Portugese;
	}

	public enum RecipeCourse {
		Main_Dishes, Desserts, Side_Dishes, Lunch, Appetizers, Salads, Breads, Breakfast_and_Brunch, Soups, Beverages, Condiments_and_Sauces, Cocktails, KidFriendly;
	}

	private Long id;
	private RecipeCuisine cuisine;
	private int prepTimeInSeconds;
	private RecipeCourse course;
	private ArrayList<String> Steps = new ArrayList<>();
	private int numberOfServings;

	public Instruction() {
	}

	public Instruction(Long id, RecipeCuisine cuisine, int prepTimeInSeconds, RecipeCourse course,
			ArrayList<String> steps, int numberOfServings) {
		super();
		this.id = id;
		this.cuisine = cuisine;
		this.prepTimeInSeconds = prepTimeInSeconds;
		this.course = course;
		Steps = steps;
		this.numberOfServings = numberOfServings;
	}



	public Instruction(RecipeCuisine cuisine, RecipeCourse course) {
		this.cuisine = cuisine;
		this.course = course;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RecipeCuisine getCuisine() {
		return cuisine;
	}

	public void setCuisine(RecipeCuisine cuisine) {
		this.cuisine = cuisine;
	}
	
	public RecipeCourse getCourse() {
		return course;
	}

	public void setCourse(RecipeCourse course) {
		this.course = course;
	}

	@Lob
	public ArrayList<String> getSteps() {
		return Steps;
	}

	public void setSteps(ArrayList<String> steps) {
		Steps = steps;
	}

	public int getPrepTimeInSeconds() {
		return prepTimeInSeconds;
	}

	public void setPrepTimeInSeconds(int prepTimeInSeconds) {
		this.prepTimeInSeconds = prepTimeInSeconds;
	}

	public int getNumberOfServings() {
		return numberOfServings;
	}

	public void setNumberOfServings(int numberOfServings) {
		this.numberOfServings = numberOfServings;
	}
	

}
