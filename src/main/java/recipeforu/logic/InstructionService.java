package recipeforu.logic;

import java.util.List;

import recipeforu.logic.Instruction.RecipeCourse;

public interface InstructionService {
	
	public Instruction addNewInstruction(Instruction instruction) throws Exception;

	List<Instruction> getInstructionByCatagory(List<RecipeCourse> courses) throws Exception;

}
