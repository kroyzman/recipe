package recipeforu.logic;
import org.springframework.stereotype.Component;

@Component
public class NewUserForm {
	
	private String email;
	private String userName;
	private String password;
	
	public NewUserForm() {
		// TODO Auto-generated constructor stub
	}
	
	public NewUserForm(String email, String userName, String password) {
		super();
		this.email=email;
		this.password = password;
		this.userName = userName;
	
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	
}

