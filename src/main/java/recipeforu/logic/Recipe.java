package recipeforu.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Recipe {

	private Long id;
	private String title;
	//private Set<Ingridient> ingridients = new HashSet<>();
	private Set<RecipeIngridient> recipeIngridients = new HashSet<>();
	private Instruction instruction;
	private ArrayList<String> img = new ArrayList<String>();
	private double avgRating;
	private int totalCountOfRatings;
	private List<CountUserWatchRecipe> views = new ArrayList<>();
	
	public Recipe() {
		this.avgRating = 0;
		this.totalCountOfRatings = 0;
	}

	
	public Recipe(Long id, String title, Set<RecipeIngridient> recipeIngridients, Instruction instruction,
			ArrayList<String> img, double avgRating) {
		super();
		this.id = id;
		this.title = title;
		this.recipeIngridients = recipeIngridients;
		this.instruction = instruction;
		this.img = img;
		this.avgRating = avgRating;
		this.totalCountOfRatings = 0;
	}


	public Recipe(String title, Set<RecipeIngridient> recipeIngridients, Instruction instruction) {
		this.title = title;
		this.recipeIngridients = recipeIngridients;
		this.instruction = instruction; 
		this.totalCountOfRatings = 0;
		this.avgRating = 0;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@JsonIgnore
	@OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<RecipeIngridient> getRecipeIngridients() {
		return recipeIngridients;
	}

	public void setRecipeIngridients(Set<RecipeIngridient> recipeIngridients) {
		this.recipeIngridients = recipeIngridients;
	}
	
	
	@OneToOne
	public Instruction getInstruction() {
		return instruction;
	}
	
	public void setInstruction(Instruction instruction) {
		this.instruction = instruction;
	}

	public ArrayList<String> getImg() {
		return img;
	}

	public void setImg(ArrayList<String> img) {
		this.img = img;
	}

	public double getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(double rating) {
		this.avgRating = rating;
	}


	public int getTotalCountOfRatings() {
		return totalCountOfRatings;
	}


	public void setTotalCountOfRatings(int totalCountOfRatings) {
		this.totalCountOfRatings = totalCountOfRatings;
	}

	@ManyToMany(cascade = CascadeType.PERSIST)
	public List<CountUserWatchRecipe> getViews() {
		return views;
	}


	public void setViews(List<CountUserWatchRecipe> views) {
		this.views = views;
	}
	
	
	
	
}
