package recipeforu.logic;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "recipes_ingridients")
public class RecipeIngridient implements Serializable{

	private Recipe recipe;
	private Ingridient ingridient;
	private double quantity;
	private String unit;
	private List<Ingridient> optionalIngridientsForReplace;

	public RecipeIngridient() {
		// TODO Auto-generated constructor stub
	}

	public RecipeIngridient(Recipe recipe, Ingridient ingridient, double quantity, String unit,
			List<Ingridient> optionalIngridientsForReplace) {
		super();
		this.recipe = recipe;
		this.ingridient = ingridient;
		this.quantity = quantity;
		this.unit = unit;
		this.optionalIngridientsForReplace = optionalIngridientsForReplace;
	}
	
	@Id
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "recipe_id") 
	public Recipe getRecipe() {
		return recipe;
	}
	
	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}
	
	@Id
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ingridient_id") 
	public Ingridient getIngridient() {
		return ingridient;
	}

	public void setIngridient(Ingridient ingridient) {
		this.ingridient = ingridient;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	@OneToMany(targetEntity = Ingridient.class)
	public List<Ingridient> getOptionalIngridientsForReplace() {
		return optionalIngridientsForReplace;
	}

	public void setOptionalIngridientsForReplace(List<Ingridient> optionalIngridientsForReplace) {
		this.optionalIngridientsForReplace = optionalIngridientsForReplace;
	}

}
