package recipeforu.logic;

import java.util.List;

public interface RecipeService{

	public Recipe addNewRecipe(Recipe recipe) throws Exception;
	public  List<Recipe> getRecipesByTitle(String title, int page, int size);
	public List<Recipe> getAllRecipes(int size, int page);
	public List<Recipe> viewReciepsByTitle(int page, int size,String title);
	public Recipe getReciepById(int id) throws Exception;
	public long getRecipesCount() throws Exception;
	public List<Recipe> getRecipesByCatagory(int page, int size, String[] myParams);
	public List<Recipe> getRecipesByInstructions(int page, int size, List<Instruction> instructionsList);

	public List<Recipe> getRecipesByIngridients(List<Ingridient> ingridients);
	

	public List<Recipe> getRecipesByInstructionsAndTitle(int page, int size, List<Instruction> instructionsList, String title);
	public Recipe updateRating(Recipe recipe, int rating, UserDetails user, Long totalRatesCountForRecipe);
	public long getRecipesCountByTitle(String title);
	public void increaseCountViews(Long recommendUserId, int recipeId )throws Exception;
	public List getAllRecipesWithoutPagination();
	public void save(Recipe currentRecipe);

}
