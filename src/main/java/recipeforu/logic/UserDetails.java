package recipeforu.logic;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


@Entity
public class UserDetails {

	private String userName;
	private Long recommendUserId;
	private String password;
	private String email;
	private List<Ingridient> ingridients=new ArrayList<>() ;
	private List<Recipe> favouriteRecipes=new ArrayList<>();
	private List<UserRatesRecipe> userRatedRecipes= new ArrayList<>();	
	private static Long recommendCounter = 0L;
	
	public UserDetails() {
		this.recommendUserId = recommendCounter;
		recommendCounter++;
	}

	public UserDetails(String email, String userName, String password) {
		super();
		this.email = email;
		this.userName = userName;
		this.password = password;
		this.recommendUserId = recommendCounter;
		recommendCounter++;

	}

	public UserDetails(String email, String userName, String password, List<Ingridient> ingridients,  List<Recipe> favouriteRecipes, List<UserRatesRecipe> UserRatedRecipes) {
		super();
		this.email = email;
		this.userName = userName;
		this.password = password;
		this.ingridients= ingridients;
		this.favouriteRecipes= favouriteRecipes;
		this.userRatedRecipes=UserRatedRecipes;
		this.recommendUserId = recommendCounter;
		recommendCounter++;

	}

	@Id
	@Column(name = "EMAIL", nullable = false, columnDefinition="VARCHAR(64)")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getRecommendUserId() {
		return recommendUserId;
	}

	public void setRecommendUserId(Long recommendUserId) {
		this.recommendUserId = recommendUserId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}	


	@ManyToMany(targetEntity = Ingridient.class)
	public List<Ingridient> getIngridients() {
		return ingridients;
	}

	public void setIngridients(List<Ingridient> ingridients) {
		this.ingridients = ingridients;
	}

	@OneToMany(targetEntity = Recipe.class)
	public List<Recipe> getFavouriteRecipes() {
		return favouriteRecipes;
	}

	public void setFavouriteRecipes(List<Recipe> favouriteRecipes) {
		this.favouriteRecipes = favouriteRecipes;
	}
	
	@OneToMany(targetEntity = UserRatesRecipe.class, cascade = CascadeType.MERGE, orphanRemoval = true)
	public List<UserRatesRecipe> getUserRatesRecipes() {
		return userRatedRecipes;
	}
	
	public void setUserRatesRecipes(List<UserRatesRecipe> userRatesRecipes) {
		this.userRatedRecipes=userRatesRecipes;
	}
	
	





}
