package recipeforu.logic;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "user_rates_recipe")
public class UserRatesRecipe {
	
	private Long rateId;
	private int userRate;
	private Long ratedRecipeId;
	
	public UserRatesRecipe()	{
	}
	
	public UserRatesRecipe( int userRate, Long ratedRecipeId){
		super();
		this.setUserRate(userRate);
		this.ratedRecipeId=ratedRecipeId;
	}
	
	public UserRatesRecipe(Long ratedRecipeId) {//user didnt rated a recipe
	
		this.userRate=0;
		this.ratedRecipeId=ratedRecipeId;		
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getRateId() {
		return rateId;
	}

	public void setRateId(Long rateId) {
		this.rateId = rateId;
	}

	public int getUserRate() {
		return userRate;
	}	
	
	public void setUserRate(int userRate)  {
		if (userRate<=5 && userRate>=1)
			this.userRate=userRate;
		else 
			this.userRate=0;
	}
	
	
	public Long getRatedRecipeId() {
		return ratedRecipeId;
	}
	
	public void setRatedRecipeId(Long ratedRecipeId) {
		this.ratedRecipeId = ratedRecipeId;
	}
	
}
