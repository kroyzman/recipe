package recipeforu.logic.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import recipeforu.data.IngridientDao;
import recipeforu.logic.Ingridient;
import recipeforu.logic.IngridientService;
import recipeforu.logic.Recipe;

@Service("JpaIngridientService")
public class JpaIngridientService implements IngridientService{
	private IngridientDao ingridients;
	
	@Autowired
	public JpaIngridientService(IngridientDao ingridients) {
		super();
		this.ingridients = ingridients;
	}
	
	@Override
	@Transactional
	public Ingridient addNewIngridient(Ingridient ingridient) throws Exception {
		return this.ingridients.save(ingridient);

	}
	
	@Override
	@Transactional
	public Ingridient[] getIngridientsByName(String name) {
		return this.ingridients.getIngridientsByName(name);
	}
	
	@Override
	@Transactional
	public Ingridient getIngridientsByExactName(String name) {
		return this.ingridients.getIngridientsByExactName(name);
	}
	
	@Override
	@Transactional
	public Ingridient getIngridientById(long id) throws Exception {
		return this.ingridients.findById(id).orElseThrow(() -> new Exception("No Such Ingridient"));
	}
	
	@Override
	@Transactional
	public List<Ingridient> getAllIngridients() throws Exception {
		return this.ingridients.findAll();
	}
	
}
