package recipeforu.logic.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import recipeforu.data.InstructionDao;
import recipeforu.data.RecipeDao;
import recipeforu.logic.Instruction;
import recipeforu.logic.InstructionService;
import recipeforu.logic.Recipe;
import recipeforu.logic.Instruction.RecipeCourse;

@Service("JpaInstructionService")
public class JpaInstructionService implements InstructionService {
	private InstructionDao instructions;

	@Autowired
	public JpaInstructionService(InstructionDao instructions) {
		this.instructions = instructions;
	}

	@Override
	@Transactional
	public Instruction addNewInstruction(Instruction instruction) throws Exception {
	
		return this.instructions.save(instruction);

	}
	
	@Override
	@Transactional
	public List<Instruction> getInstructionByCatagory(List<RecipeCourse> recipeCourse) throws Exception {
		
//		return (List<Instruction>) this.instructions.findAll();
		System.out.println(recipeCourse + " in instruction");
		return this.instructions.getInstructionByCatagory(recipeCourse);

	}
	

}
