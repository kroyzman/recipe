//package recipeforu.logic.jpa;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import recipeforu.data.RecipeIngridientsDao;
//import recipeforu.logic.Ingridient;
//import recipeforu.logic.RecipeIngridientsService;
//
//@Service("JpaRecipeIngridients")
//public class JpaRecipeIngridients implements RecipeIngridientsService {
//	
//	private RecipeIngridientsDao recipeIngridients;
//	
//	@Autowired
//	public void setRecipeIngridients(RecipeIngridientsDao recipeIngridients) {
//		this.recipeIngridients = recipeIngridients;
//	}
//
//	@Override
//	public List<Long> getRecipesIdsByIngridients(List<Ingridient> list) {
//		return this.recipeIngridients.getRecipesIdsByIngridients(list);
//	}
//	
//	
//
//	
//}
