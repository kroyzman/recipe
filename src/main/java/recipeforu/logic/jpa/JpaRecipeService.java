package recipeforu.logic.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.data.domain.PageRequest;
import recipeforu.data.RecipeDao;
import recipeforu.logic.CountUserWatchRecipe;
import recipeforu.logic.Ingridient;
import recipeforu.logic.Instruction;
import recipeforu.logic.Recipe;
import recipeforu.logic.RecipeService;
import recipeforu.logic.UserDetails;

@Service("JpaRecipeService")
public class JpaRecipeService implements RecipeService {

	private RecipeDao recipes;
	
	@Autowired
	public JpaRecipeService(RecipeDao recipes) {
		this.recipes = recipes;
	}

	@Override
	@Transactional
	public Recipe addNewRecipe(Recipe recipe) throws Exception {
		return this.recipes.save(recipe);
	}
	
	@Override
	@Transactional
	public long getRecipesCount() throws Exception {
		return this.recipes.count();
	}	

	@Override
	@Transactional
	
	public  List<Recipe> getRecipesByTitle(String title,int page,int size) {
		return this.recipes.getRecipesByTitle(title, PageRequest.of(page, size));
	}

	@Override
	@Transactional
	public List<Recipe> getAllRecipes(int page, int size) {
		
		return this.recipes.findAll(PageRequest.of(page, size)).getContent();
   	}
	
	/*@Override
	@Transactional
	public List<Recipe> getRecipesByCatagory(int page, int size, String[] myParams) {
		
		return this.recipes.findAll(PageRequest.of(page, size)).getContent();
   	}
	*/

	@Override
	public List<Recipe> viewReciepsByTitle(int page, int size,String title) {
		
		//.return this.recipes.getRecipesByTitle(PageRequest.of(page, size),title)).ge;
		//Stream reciepsStream = Stream.of(this.recipes.getRecipesByTitle(PageRequest.of(page, size),title));
		//List reciepsList = (ArrayList) reciepsStream.collect(Collectors.toList());
		//return reciepsList;
		
		org.springframework.data.domain.Pageable first = PageRequest.of(page, size);
		return this.recipes.getRecipesByTitle(title, first);
			
	}

	@Override
	public Recipe getReciepById(int id) throws Exception {
		return this.recipes.findById((long) id).orElseThrow(() -> new Exception("No Such Reciep"));
	}

	@Override
	public List<Recipe> getRecipesByInstructions(int page, int size, List<Instruction> instructionsList) {
		org.springframework.data.domain.Pageable first = PageRequest.of(page, size);
		return this.recipes.getRecipesByInstructions(instructionsList, first);
	}

	@Override

	public List<Recipe> getRecipesByIngridients(List<Ingridient> ingridients) {
		return this.recipes.getRecipesByIngridients(ingridients);
	}


	public List<Recipe> getRecipesByCatagory(int page, int size, String[] myParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Recipe> getRecipesByInstructionsAndTitle(int page, int size, List<Instruction> instructionsList, String title) {
		org.springframework.data.domain.Pageable first = PageRequest.of(page, size);
		return this.recipes.getRecipesByInstructionsAndTitle(instructionsList, title, first);
	}

	@Override
	public Recipe updateRating(Recipe recipe, int rating, UserDetails user, Long totalRatesCountForRecipe) {
		boolean isRatedBefore = false;
		
		
		recipe.setTotalCountOfRatings(totalRatesCountForRecipe.intValue());
		System.out.println("old average: " + recipe.getAvgRating() + " old count: " + (recipe.getTotalCountOfRatings() - 1) + " new rating: " + rating);
		recipe.setAvgRating((recipe.getAvgRating() * (recipe.getTotalCountOfRatings() - 1) + rating) / recipe.getTotalCountOfRatings());
		
		return this.recipes.save(recipe);
	}

	@Override
	public long getRecipesCountByTitle(String title) {
		return this.recipes.getRecipesCountByTitle(title);
	}

	@Override
	public void increaseCountViews(Long recommendUserId, int recipeId) throws Exception {
		boolean isCountedBefore = false;
		Recipe currentRecipe = this.getReciepById(recipeId);
		for(int i = 0 ; i < currentRecipe.getViews().size() ; i++) {
			if(currentRecipe.getViews().get(i).getUserId() == recommendUserId) {
				isCountedBefore = true;
				currentRecipe.getViews().get(i).setCountViews(currentRecipe.getViews().get(i).getCountViews() + 1);
				break;
			}
		}
		
		if(!isCountedBefore) {
			CountUserWatchRecipe views = new CountUserWatchRecipe();
			views.setCountViews(1);
			views.setUserId(recommendUserId);
			currentRecipe.getViews().add(views);
		}
		
		
		this.recipes.save(currentRecipe);
	}

	@Override
	public List getAllRecipesWithoutPagination() {
		return this.recipes.findAll();
	}

	@Override
	public void save(Recipe currentRecipe) {
		this.recipes.save(currentRecipe);
		
	}

}
