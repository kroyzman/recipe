package recipeforu.logic.jpa;

import recipeforu.logic.jpa.UserAlreadyExistsException;

import java.util.List;

import recipeforu.logic.UserDetails;

public interface UserService {

	public void cleanup();

	public UserDetails addNewUser(String email, String username, String password) throws UserAlreadyExistsException;

	public UserDetails getUserByEmail(String email) throws UserNotFoundException;
	
	public void updateUser(String email, UserDetails newValues) throws UserNotFoundException;
	
	public UserDetails login(String email, String password) throws UserNotFoundException;

	public void deleteByEmail(String email) throws UserNotFoundException;

	public Long getRatedRecipeCount(Long recipeId);

	public List<UserDetails> getAllUsers();
	


	//public UserEntity verifyUser(String email, String playground, String code) throws UserNotFoundException, UserUnverifiedException;
	
	//public void validateUserPlayground(String playground) throws UserIllegalInputException; 
	
	//public void validateUserEmail(String email) throws UserIllegalInputException;
	
	//public void validateUserRole(String role) throws UserIllegalInputException;

}
