package recipeforu.logic.jpa;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import recipeforu.data.UserDao;
import recipeforu.logic.UserDetails;

@Service
public class jpaUserService implements UserService {

	/** Class Attributes */
	private UserDao users;

	@Autowired
	public jpaUserService(UserDao users) {
		this.users = users;		
	}

	@Override
	public void cleanup() {
		users.deleteAll();

	}

	@Override
	@Transactional
	public UserDetails addNewUser(String email, String username, String password) throws UserAlreadyExistsException{
		if (!this.users.existsById(email)) {
			return users.save(new UserDetails(email, username, password));
		}
		else {
			throw new UserAlreadyExistsException("User exists with email: " + email);
		}		
	}

	@Override
	public void updateUser(String email, UserDetails newValues) throws UserNotFoundException {
		UserDetails existingUser = this.getUserByEmail(email);

		if (newValues.getUserName() != null && !newValues.getUserName().isEmpty()
				&& newValues.getUserName()!=existingUser.getUserName()) 
			existingUser.setUserName(newValues.getUserName());

		if (newValues.getPassword()!= null 	&& !newValues.getPassword().isEmpty()
				&&newValues.getPassword()!=existingUser.getPassword())
			existingUser.setPassword(newValues.getPassword());

		if (newValues.getIngridients() != null
				&& !newValues.getIngridients().equals(existingUser.getIngridients())) {
			existingUser.getIngridients().clear();
			existingUser.setIngridients(newValues.getIngridients());
		}
			

		if (newValues.getFavouriteRecipes() != null && !newValues.getFavouriteRecipes().isEmpty()) {
			System.out.println("IN HERHERHERHEHRE: favourite");
			existingUser.setFavouriteRecipes(newValues.getFavouriteRecipes());
		}		

		if (newValues.getUserRatesRecipes() == null && !newValues.getUserRatesRecipes().isEmpty()
				&& !newValues.getUserRatesRecipes().equals(existingUser.getUserRatesRecipes())) {
			System.out.println("IN HERHERHERHEHRE");
			
		}
		
		System.out.println("Saving user....");
		this.users.save(existingUser);

	}

	@Override
	public UserDetails login(String email, String password) throws UserNotFoundException {
		UserDetails user = getUserByEmail(email);
		
		if(user.getPassword().equals(password)) {
			return user;
		} else {
			throw new UserNotFoundException("Wrong credentials");
		}

	}
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails getUserByEmail(String email) throws UserNotFoundException {

		return users.findById(email).orElseThrow(() -> new UserNotFoundException(
				"User doesn't exists with email: " + email ));

	}

	@Override
	@Transactional
	public void deleteByEmail(String email) throws UserNotFoundException  {
		this.users.delete(this.getUserByEmail(email));		
	}
	
	@Override
	@Transactional
	public Long getRatedRecipeCount(Long recipeId) {
		return this.users.getRatedRecipeCount(recipeId);
		
	}

	@Override
	public List<UserDetails> getAllUsers() {
		return (List<UserDetails>) this.users.findAll();
	}


}
